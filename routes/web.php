<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/logout', function(){
	\Auth::logout();
	return redirect(url('/'));
});

Route::get('/home', function(){
	return redirect(url('/'));
});

Route::group(['middleware' => ['web', 'auth']], function(){
 Route::get('/', function(){
 	$role = \App\User::find(Auth::user()->id)->roles()->first();
 	if ($role->name == "superuser") {
 		return redirect(url('superuser'));
 	}
 	else if ($role->name == "admin"){
 		return redirect(url('admin'));
 	}
 	else{
 		return redirect(url('user'));
 	}
 });
});

Route::group(['prefix' => 'superuser', 'middleware' => ['web', 'auth', 'role:superuser']], function(){
 Route::get('/', 'SuperuserController@index');
 Route::get('activity', 'SuperuserController@activity_page');
 Route::group(['prefix' => 'manage'], function(){
  Route::group(['prefix' => 'user'], function(){
 	Route::get('/', 'SuperuserController@user_list');
 	Route::get('create', 'SuperuserController@user_add');
 	Route::post('save', 'SuperuserController@user_store');
 	Route::get('detail/{id}', 'SuperuserController@user_detail');
 	Route::get('edit/{id}', 'SuperuserController@user_edit');
 	Route::get('delete/{id}', 'SuperuserController@user_destroy');
 	Route::post('update', 'SuperuserController@user_update');
  });
   Route::group(['prefix' => 'admin'], function(){
 	Route::get('/', 'SuperuserController@admin_list');
 	Route::get('create', 'SuperuserController@admin_add');
 	Route::post('save', 'SuperuserController@admin_store');
 	Route::get('detail/{id}', 'SuperuserController@admin_detail');
 	Route::get('edit/{id}', 'SuperuserController@admin_edit');
 	Route::get('delete/{id}', 'SuperuserController@admin_destroy');
 	Route::post('update', 'SuperuserController@admin_update');
  });
 });
	Route::group(['prefix' => 'inventory'], function(){
		Route::group(['prefix' => 'stock'], function(){
		 Route::get('/', 'SuperuserController@stock_items_list');
		 Route::get('create', 'SuperuserController@stock_items_add');
		 Route::post('save', 'SuperuserController@stock_items_store');
		 Route::get('detail/{id}', 'SuperuserController@stock_items_detail');
		 Route::get('delete/{id}', 'SuperuserController@stock_items_destroy');
		 Route::post('update', 'SuperuserController@stock_items_update');
		 Route::post('new', 'SuperuserController@stock_items_new');
		 Route::get('report', 'SuperuserController@stock_items_report');
		 Route::post('report/pdf', 'SuperuserController@stock_items_report_pdf');
		});
	});
	Route::group(['prefix' => 'transaction'], function(){
		Route::get('/', 'SuperuserController@transaction_count');
    Route::get('clear', function() {
      if (session('transaksi')) {
        session()->forget('transaksi');
        session()->forget('jumlah');
        session()->forget('pajak');
        return redirect(url('superuser/transaction'));
      }
    });
		Route::get('feedback', 'SuperuserController@transaction_feedback');
	});
});

Route::group(['prefix' => 'report', 'middleware' => ['web', 'auth', 'role:superuser']], function() {
	Route::get('{role}/pdf', 'SuperuserController@PDFAccount');
	Route::get('{role}/xlsx', 'SuperuserController@XLSXAccount');
});

Route::group(['prefix' => 'api', 'middleware' => ['web', 'auth']], function(){
	Route::get('item/{code}', 'APIController@get_item');
	Route::get('get/item/{id}', 'APIController@get_input_item');
});

Route::get('/images/{filename}', function ($filename){
	$path = storage_path() . '/' .$filename;
	$file = File::get($path);
	$type = File::mimeType($path);
	$r = Response::make($file,200);
	$r->header("Content-Type", $type);
	return $r;
});
