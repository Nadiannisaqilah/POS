<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Type;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User;
        $user1->name = "Annisa Nadia Nur'aqilah";
        $user1->email = "annisaaqilah3@gmail.com";
        $user1->password = bcrypt('jakarta799');
        $user1->image = "ava1.jpg";
        $user1->gender = "Perempuan";
        $user1->phone_number = "0218015345";
        $user1->address = "Jl. Condet Balekambang Pucung 1 RT 004/02, Kelurahan Balekambang, Kecamatan Kramatjati, Jakarta Timur";
        $user1->save();

        $user2 = new User;
        $user2->name = "Nadia";
        $user2->email = "annisaqilah799@yahoo.com";
        $user2->password = bcrypt('jakarta799');
        $user2->image = "ava1.jpg";
        $user2->gender = "Perempuan";
        $user2->phone_number = "0218015345";
        $user2->address = "Jl. Condet Balekambang Pucung 1 RT 004/02, Kelurahan Balekambang, Kecamatan Kramatjati, Jakarta Timur";
        $user2->save();

        $superuser = new Role();
		$superuser->name         = 'superuser';
		$superuser->display_name = 'Master'; // optional
		$superuser->description  = 'Pengusaha'; // optional
		$superuser->save();

		$admin = new Role();
		$admin->name         = 'admin';
		$admin->display_name = 'Inventaris'; // optional
		$admin->description  = 'Pengelola Barang'; // optional
		$admin->save();

		$user = new Role();
		$user->name         = 'user';
		$user->display_name = 'Teller'; // optional
		$user->description  = 'Pengelola Transaksi'; // optional
		$user->save();

		$su = User::where('email', 'annisaaqilah3@gmail.com')->first();
		$su->attachRole($superuser);

		$ad = User::where('email', 'annisaqilah799@yahoo.com')->first();
		$ad->attachRole($admin);

        $type1 = new Type;
        $type1->nama = "Sembako";
        $type1->save();

        $type2 = new Type;
        $type2->nama = "Minuman Kemasan";
        $type2->save();

        $type3 = new Type;
        $type3->nama = "Makanan Ringan";
        $type3->save();

        $type4 = new Type;
        $type4->nama = "Kebutuhan Dapur";
        $type4->save();

        $type5 = new Type;
        $type5->nama = "Kebutuhan Kamar Mandi";
        $type5->save();

        $type6 = new Type;;
        $type6->nama = "Mie Instant";
        $type6->save();

        $type7 = new Type;;
        $type7->nama = "Kopi";
        $type7->save();

        $type8 = new Type;;
        $type8->nama = "Susu";
        $type8->save();

        $type9 = new Type;;
        $type9->nama = "Alas Kaki";
        $type9->save();

        $type10 = new Type;;
        $type10->nama = "Bahan Bakar";
        $type10->save();
    }
}
