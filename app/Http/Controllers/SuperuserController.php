<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Validator;
use Auth;
use Carbon\Carbon;
use \App\User;
use \App\Role;
use \App\RoleUSer;
use \App\Item;
use \App\Type;

class SuperuserController extends Controller
{
    public function index()
    {
        $user = \App\User::all();
        $all_users = 0;
        foreach ($user as $key => $value) {
            $users = \App\RoleUser::where('user_id', $value->id)->first();
            if ($users->role_id != 1) {
                $all_users += 1;
            }
        }
        $all_items = count(\App\Item::all());
        return view('users.master.home')->with('all_users', $all_users)->with('all_items', $all_items);
    }

    public function user_list()
    {

    	$data = User::all();
    	$users = [];
    	foreach ($data as $key => $value) {
    		$role = \App\RoleUser::where('user_id', $value->id)->first();
    		if ($role->role_id == 3) {
    			$user = User::where('id', $value->id)->first();
    			$users = array_prepend($users, $user);
    		}
    	}
    	// return $users;
    	return View::make('users.master.manage_account.user_list')->with('data', $users);
    }

    public function activity_page()
    {
        return View::make('users.master.activity');
    }

    public function user_add()
    {
    	return View::make('users.master.manage_account.user_add');
    }

    public function user_store(Request $r)
    {
    	 $message = [
        'required' => ':attribute masih kosong.',
        'email' => ':attribute tidak berformat email.',
        'min' => ':attribute tidak boleh kurang dari 6 karakter',
        'mimes' => ':attribute harus berupa .jpg, .jpeg, .bmp, atau .png'
        ];

        $validator = Validator::make($r->all(),[
            'name' => 'required',
            'email' => 'email',
            'password' => 'required|min:6',
            'user_image' => 'mimes:jpg,jpeg,bmp,png',
            ], $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

    	$data = new User;
    	$data->name = $r->input('name');
    	$data->email = $r->input('email');
    	$data->password = bcrypt($r->input('password'));
    	$data->gender = $r->input('gender');
    	if ($data->gender == "Perempuan") {
    		$data->image = "ava1.jpg";
    	}
    	else
    	{
    		$data->image = "ava2.jpg";
    	}
    	$data->phone_number = $r->input('phone_number');
    	$data->address = $r->input('address');
    	$data->save();

    	$user = Role::where('name', 'user')->first();
      	$data->attachRole($user->id);

    	return redirect(url('superuser/manage/user'));
    }

    public function user_update(Request $r)
    {
         $message = [
        'required' => ':attribute masih kosong.',
        'email' => ':attribute tidak berformat email.',
        'min' => ':attribute tidak boleh kurang dari 6 karakter',
        'mimes' => ':attribute harus berupa .jpg, .jpeg, .bmp, atau .png'
        ];

        $validator = Validator::make($r->all(),[
            'name' => 'required',
            'email' => 'email',
            'password' => 'required|min:6',
            'user_image' => 'mimes:jpg,jpeg,bmp,png',
            ], $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
                $data = User::where('id', $r->input('id'))->first();
                $data->name = $r->input('name');
                $data->email = $r->input('email');
                $data->password = bcrypt($r->input('password'));
                    $a = $r->file('image');
                    $name = date("YmdHis").$r->input('nama').".".$a->getClientOriginalExtension();
                    $a->move(storage_path(),$name);
                    $data->image = $name;
                $data->gender = $r->input('gender');
                $data->address = $r->input('address');
                $data->phone_number = $r->input('phone_number');
                $data->save();

                return redirect(url('superuser/manage/user/detail/'.$data->id));
    }

    public function user_detail($id)
    {
        $data = User::find($id);
        return View::make('users.master.manage_account.user_detail')->with('data', $data);
    }

    public function user_edit($id)
    {
        $data = User::find($id);
        return View::make('users.master.manage_account.user_edit')->with('data', $data);
    }

    public function user_destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        return redirect(url('superuser/manage/user'));
    }

    public function admin_list()
    {

        $data = User::all();
        $users = [];
        foreach ($data as $key => $value) {
            $role = \App\RoleUser::where('user_id', $value->id)->first();
            if ($role->role_id == 2) {
                $user = User::where('id', $value->id)->first();
                $users = array_prepend($users, $user);
            }
        }
        // return $users;
        return View::make('users.master.manage_account.admin_list')->with('data', $users);
    }

    public function admin_add()
    {
        return View::make('users.master.manage_account.admin_add');
    }

    public function admin_store(Request $r)
    {
         $message = [
        'required' => ':attribute masih kosong.',
        'email' => ':attribute tidak berformat email.',
        'min' => ':attribute tidak boleh kurang dari 6 karakter',
        'mimes' => ':attribute harus berupa .jpg, .jpeg, .bmp, atau .png'
        ];

        $validator = Validator::make($r->all(),[
            'name' => 'required',
            'email' => 'email',
            'password' => 'required|min:6',
            'user_image' => 'mimes:jpg,jpeg,bmp,png',
            ], $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = new User;
        $data->name = $r->input('name');
        $data->email = $r->input('email');
        $data->password = bcrypt($r->input('password'));
        $data->gender = $r->input('gender');
        if ($data->gender == "Perempuan") {
            $data->image = "ava1.jpg";
        }
        else
        {
            $data->image = "ava2.jpg";
        }
        $data->phone_number = $r->input('phone_number');
        $data->address = $r->input('address');
        $data->save();

        $user = Role::where('name', 'admin')->first();
        $data->attachRole($user->id);

        return redirect(url('superuser/manage/admin'));
    }

    public function admin_update(Request $r)
    {
        $message = [
            'required' => ':attribute masih kosong.',
            'email' => ':attribute tidak berformat email.',
            'min' => ':attribute tidak boleh kurang dari 6 karakter',
            'mimes' => ':attribute harus berupa .jpg, .jpeg, .bmp, atau .png'
        ];

        $validator = Validator::make($r->all(),[
            'name' => 'required',
            'email' => 'email',
            'password' => 'required|min:6',
            'user_image' => 'mimes:jpg,jpeg,bmp,png',
            ], $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
                $data = User::where('id', $r->input('id'))->first();
                $data->name = $r->input('name');
                $data->email = $r->input('email');
                $data->password = bcrypt($r->input('password'));
                    $a = $r->file('image');
                    $name = date("YmdHis").$r->input('nama').".".$a->getClientOriginalExtension();
                    $a->move(storage_path(),$name);
                    $data->image = $name;
                $data->gender = $r->input('gender');
                $data->address = $r->input('address');
                $data->phone_number = $r->input('phone_number');
                $data->save();

                return redirect(url('superuser/manage/admin/detail/'.$data->id));
    }

    public function admin_detail($id)
    {
        $data = User::find($id);
        return View::make('users.master.manage_account.admin_detail')->with('data', $data);
    }

    public function admin_edit($id)
    {
        $data = User::find($id);
        return View::make('users.master.manage_account.admin_edit')->with('data', $data);
    }

    public function admin_destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        return redirect(url('superuser/manage/admin'));
    }

    public function PDFAccount($role)
    {
        if ($role == "teller") {
            $roleUser = RoleUser::where('role_id', '3')->get();
            $level = "Teller";
        }
        else if ($role == "inventaris") {
            $roleUser = RoleUser::where('role_id', '2')->get();
            $level = "Inventaris";
        }

        $data = [];
        foreach ($roleUser as $key => $value) {
            $users = User::where('id', $value->user_id)->first();
            $data = array_prepend($data, $users);
        }

        $pdf = \PDF::loadView('users.master.manage_account.pdfaccount', ['data' => $data, 'level' => $level])->setPaper('a4', 'landscape')->setOptions(['dpi' => 150, 'defaultFont' => 'Sans-serif']);
            return $pdf->stream($role.'.pdf');
    }

    public function XLSXAccount($role)
    {
        if ($role == "teller") {
            $roleUser = RoleUser::where('role_id', '3')->get();
            $level = "Teller";
        }
        else if ($role == "inventaris") {
            $roleUser = RoleUser::where('role_id', '2')->get();
            $level = "Inventaris";
        }
         $data = [];
        foreach ($roleUser as $key => $value) {
            $users = User::select('users.name','users.email','users.gender','users.address','users.phone_number')->where('id', $value->user_id)->first()->toArray();
            $data = array_prepend($data, $users);
        }

          return \Excel::create('Data '.$level, function($excel) use ($data)
          {
              $excel->sheet('mySheet', function($sheet) use ($data)
              {
                  $sheet->fromArray($data);
              });
          })->download('xlsx');
    }

    public function stock_items_list(Request $r)
    {
        $data = Item::all();
        $item = [];

        foreach ($data as $key => $value) {
            $item[$key] = $value->barang_nama;
        }

        $item = array_unique($item);

        foreach ($data as $key => $value) {
            if (!array_key_exists($key, $item)) {
            unset($data[$key]);
            }
        }

        foreach ($data as $key => $value) {
            $data[$key]['count'] = count(Item::where('barang_nama', $value->barang_nama)->get());
        }
        return View::make('users.master.inventory.stock_item_list')->with('data', $data);
    }

    public function stock_items_add()
    {
        $type = Type::all();
        return View::make('users.master.inventory.stock_item_add')->with('type', $type);
    }

    public function stock_items_store(Request $r)
    {

      $check = Item::where('barang_nama', $r->input('barang_nama'))->get();
      if (count($check)==0) {
        $data = new Item;
        $data->barang_nama = $r->input('barang_nama');
        $data->barang_kode = $r->input('barang_kode');
        $data->barang_harga = $r->input('barang_harga');
        $data->barang_jenis = $r->input('barang_jenis');
        $data->barang_isi = $r->input('barang_isi');
        $data->barang_stok = 1;
        $a = $r->file('barang_gambar');
        $name = date("YmdHis").$r->input('nama').".".$a->getClientOriginalExtension();
        $a->move(storage_path(),$name);
        $data->barang_gambar = $name;
        $data->user_id = Auth::user()->name;
        $data->save();
      }
      else {
        $data = new Item;
        $data->barang_nama = $r->input('barang_nama');
        $data->barang_kode = $r->input('barang_kode');
        $data->barang_harga = $r->input('barang_harga');
        $data->barang_jenis = $r->input('barang_jenis');
        $data->barang_isi = $r->input('barang_isi');
        $data->barang_stok = $check[0]->barang_stok + 1;
        $a = $r->file('barang_gambar');
        $name = date("YmdHis").$r->input('nama').".".$a->getClientOriginalExtension();
        $a->move(storage_path(),$name);
        $data->barang_gambar = $name;
        $data->user_id = Auth::user()->name;
        $data->save();
      }


      return redirect(url('superuser/inventory/stock'));
    }

    public function stock_items_detail($id)
    {
        $check = Item::where('id', $id)->first();
        if (count($check) == 0) {
            abort('404');
        }
        $data = Item::where('barang_nama', $check->barang_nama)->get();
        return View::make('users.master.inventory.stock_item_detail')->with('data', $data)->with('data1', $check);
    }

    public function stock_items_destroy($id)
    {
      $data = Item::find($id);
      $check = Item::where('barang_nama', $data->barang_nama)->get();
      foreach ($check as $key => $value) {
        $value->barang_stok -= 1;
        $value->save();
      }

      $data->delete();
      return redirect(url('superuser/inventory/stock'));
    }

    public function stock_items_update(Request $r)
    {
        $data = Item::where('id', $r->input('id'))->first();
        $data->barang_nama = $r->input('barang_nama');
        $data->barang_kode = $r->input('barang_kode');
        $data->barang_harga = $r->input('barang_harga');
        $data->barang_jenis = $r->input('barang_jenis');
        $data->barang_isi = $r->input('barang_isi');
            $a = $r->file('barang_gambar');
            $name = date("YmdHis").$r->input('nama').".".$a->getClientOriginalExtension();
            $a->move(storage_path(),$name);
            $data->barang_gambar = $name;
        $data->user_id = Auth::user()->name;
        $data->save();

        return redirect(url('superuser/inventory/stock/detail/'.$data->id));
    }

    public function stock_items_new(Request $r)
    {
      $message = [
          'required' => 'Masih ada data yang kosong.'
      ];

      $validator = Validator::make($r->all(),[
          'barang_kode' => 'required',
      ], $message);

      if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
      }

      $data1 = Item::where('id', $r->input('id'))->first();

      $data = new Item;
      $data->barang_nama = $data1->barang_nama;
      $data->barang_kode = $r->input('barang_kode');
      $data->barang_jenis = $data1->barang_jenis;
      $data->barang_isi = $data1->barang_isi;
      $data->barang_stok = $data1->barang_stok + 1;
      $data->barang_gambar = $data1->barang_gambar;
      $data->barang_harga = $data1->barang_harga;
      $data->user_id = Auth::user()->name;
      $data->save();

      $check = Item::where('barang_nama', $data1->barang_nama)->get();
      foreach ($check as $key => $value) {
        $value->barang_stok = $data1->barang_stok + 1;
        $value->save();
      }

      return redirect(url('superuser/inventory/stock/detail/'.$data1->id));
    }

    public function stock_items_report()
    {
        return View::make('users.master.inventory.stock_item_report');
    }

    public function stock_items_report_pdf(Request $r)
    {
        $bulan = $r->input('bulan');
        $data = Item::orderBy('created_at', 'ASC')->whereMonth('created_at', $bulan)->get();
        $pdf = \PDF::loadView('users.master.inventory.pdf_item', ['data' => $data, 'bulan' => $bulan])->setPaper('a4', 'landscape')->setOptions(['dpi' => 150, 'defaultFont' => 'Sans-serif']);
        return $pdf->stream('Rekapitulasi Data Barang Bulan Ke-'.$bulan.'.pdf');
    }

    public function transaction_count()
    {
        return View::make('users.master.teller.transaksi');
    }
}
