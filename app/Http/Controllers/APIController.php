<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Item;
use App\Transaction;
class APIController extends Controller
{
    public function get_item(Request $r, $code)
    {
      $response = [];

      $data = Item::where('barang_kode', $code)->first();
      if (count($data) == 0) {
        $response['success'] = 0;
        $response['message'] = "Barang tidak ditemukan.";
        return response()->json($response);
      }

      $response['success'] = 1;
      $response['barang'] = $data;
      return response()->json($response);
    }
    public function get_input_item($id)
    {
      $response = [];
        $count = 0;
        $jumlah = 0;
        $pajak = 0;

        $stok = 0;

        $data = Item::where('barang_kode', $id)->first();
        if (count($data) == 0) {
            $response['success'] = 0;
            $response['message'] = "Barang tidak ditemukan.";
            return response()->json($response);
        }


        // session()->forget('transaksi'); return;

        if (session('transaksi')) {
          foreach (session('transaksi') as $key => $value) {
            if ($value->barang_kode == $data->barang_kode) {
              $stok += 1;
            }
          }

          if ($stok > $data->barang_stok) {
            $response['success'] = 0;
            $response['message'] = "Stok habis untuk didata.";
            return response()->json($response);
          }
        }

        session()->push('transaksi', $data);
        $array = session('transaksi');
        foreach ($array as $key => $value) {
          if ($value->barang_nama == $data->barang_nama) {
            $count = $count + 1;
          }
        }

        foreach ($array as $key => $value) {
          if ($value->barang_nama == $data->barang_nama) {
            $array[$key]['count'] = $count;
          }
        }

        foreach ($array as $key => $value) {
          $tmp[$key] = $value->barang_nama;
        }

        $tmp = array_unique($tmp);

        foreach ($array as $key => $value) {
          if (!array_key_exists($key, $tmp)) {
            unset($array[$key]);
          }
        }

        foreach ($array as $key => $value) {
          $jumlah = 0;
          $pajak = 0;
          $jumlah = $value->barang_harga * $value->count;
          $pajak = $jumlah/10;

          $array[$key]['jumlah'] = $jumlah;
          $array[$key]['pajak'] = $pajak;
        }

        $all_jumlah = 0;
        $all_pajak = 0;
        foreach ($array as $key => $value) {
          $all_jumlah = $all_jumlah + $value->jumlah;
          $all_pajak = $all_pajak + $value->pajak;
        }
        $all_jumlah = $all_jumlah + $all_pajak;

        session()->put('jumlah', $all_jumlah);
        session()->put('pajak', $all_pajak);

        $response['success'] = 1;
        $response['barang'] = $array;
        $response['total'] = $all_jumlah;
        $response['pajak'] = $all_pajak;
        return response()->json($response);
    }
}
