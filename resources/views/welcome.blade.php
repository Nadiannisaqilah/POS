@extends('layouts.app-welcome')
@section('content')
<div class="container-fluid content">
 <div class="row default-style">
    <div id="left-block" class="col-md-6 col-sm-12 left-block">
        <div class="left-content">
           <h1>Retail'POS</h1>
            <img src="{{url('/landing/assets/images/retail.png')}}" alt="patience">
            <div class="timing">
                <div id="count-down" data-date="2016-10-20 00:00:00">
                    
                </div>
            </div>
        </div>                
    </div>

    <div id="right-block" class="col-md-6 col-sm-12 right-block">
        <div class="right-content">
            <h2>Selamat Datang!</h2>
            <span class="label">Silahkan Login:</span>
            <div class="row">
              <div class="col-sm-offset-2 col-sm-8">
                <form class="newsletter-signin" role="form" method="GET" action="{{url('/login')}}">
                  <div class="input-group">
                    @if(Route::has('login'))
                    <span class="input-group-btn">
                    <!-- <a href="http://fb.com/CodePassenger/"><i class="fa fa-facebook"></i></a> -->
                      <input type="submit" class="btn btn-default btn-submit" value="LOGIN">
                    </span>
                    @endif
                  </div><!-- /input-group -->
                        <p class="newsletter-success"></p>
                      <p class="newsletter-error"></p>
                  <div class="alert alert-success alert-dismissible" role="alert">                              
                       
                   </div>
                   <div class="alert alert-warning alert-dismissible" role="alert">                              
                       
                   </div>
                </form>
              </div>
            </div>
            <p class="followus">Sosial Media</p>
            <ul class="social-icon">
                <li><a href="https://github.com/Nadiannisaqilah"><i class="fa fa-github"></i></a></li>
                <li><a href="https://www.facebook.com/annisa.aqilah.12"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/Nadiannisaa7"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://plus.google.com/u/0/112536038831361144252"><i class="fa fa-google"></i></a></li>
            </ul>
        </div>
        <!-- /.contact-blcok -->              
    </div>
    <!-- ./left-content -->
 </div>
</div>
@endsection