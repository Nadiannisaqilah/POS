@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Barang Beli
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Barang</a></li>
        <li class="active"> Barang Beli</li>
      </ol>
    </section>
  <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Input Barang Baru</h3>
        </div>
        <form class="form-horizontal" method="POST" action="{{ url('superuser/inventory/stock/report/pdf') }}" enctype="multipart/form-data"> 
        {{ csrf_field() }}
        <div class="box-body">
        <div class="form-group">
            <div class="col-sm-12">
              <select class="form-control input-lg" name="bulan">
                <option disabled="" selected="">Pilih Bulan</option>
                <option value="1">Januari</option>
                <option value="2">Februari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>
            </div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" id="buttonTambah" class="btn bg-red pull-right"><i class="fa fa-file-pdf-o margin-r-5"></i> Report</button>
      </div>
      </form>
    </div>
  </div>
</div>
</section>
</div>
@endsection