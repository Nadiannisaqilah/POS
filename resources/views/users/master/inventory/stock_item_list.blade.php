@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Barang Beli
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-database"></i> Kelola Barang</a></li>
        <li class="active">Barang Beli</li>
      </ol>
    </section>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box">
<div class="box-header">
  <h3 class="box-title">List Data Barang Beli</h3>
  <a href="{{ url('superuser/inventory/stock/create') }}" class="btn bg-blue" style="float: right;">Tambah Baru</a>
  <a href="{{ url('#') }}" id="buttonModel" class="btn btn-default" style="float: right; margin-right: 2px;">Tambah Stok</a>
</div>
<div class="box-body">
  <table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>Nama Barang</th>
      <th>Persediaan</th>
      <th>Option</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data as $key)
    <tr>
      <td>{{ $key->barang_nama }}</td>
      <td>{{ $key->count }}</td>
      <td>
      <center>
        <a href="{{ url('superuser/inventory/stock/detail/'.$key->id) }}" class="btn bg-blue">Buka</a>
      </center>
      </td>
    </tr>
    @endforeach
    </tbody>
  </table>
 </div>
</div>
</div>
</div>
<div class="example-modal">
        <div class="modal" id="modalAdd">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambahkan Persediaan </h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal" method="POST" action="{{ url('superuser/inventory/stock/new') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="itemId">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama Barang</label>
                    <div class="col-sm-3">
                      <select class="selectpicker" id="selectName" name="barang_nama" data-live-search="true">
                        <option disabled="" selected="">Pilih Nama Barang</option>
                      @foreach($data as $key)
                        <option value="{{ $key->barang_kode }}">{{ $key->barang_nama }}</option>
                      @endforeach
                      </select>
                    </div>
                </div>
                @if(count($errors) > 0)
                  @foreach($errors->all() as $error)
                  {{ $error }}
                  @endforeach
                @endif
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_kode"  placeholder="Masukkan Kode Barang">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    </div>
</section>
</div>
  <script>
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
  <script type="text/javascript">
    $(function(){
      $('#buttonModel').on('click', function(){
        $('#modalAdd').modal();
      });
      $('#selectName').on('change', function(){
        var check = $(this).val();
        $.ajax({
          url : '{{ url('api/item') }}/'+check,
          method  : 'GET',
          success: function(data){
            if (data.success == 0) {
                alert('Data tidak ditemukan');
              }
              else{
                // alert(data.barang.barang_nama);
                $("#itemId").val(data.barang.id);
              }
          }
        })
      });
    });
  </script>
@endsection
