<!DOCTYPE html>
<html>
<head>
</head>
<style>
  table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
  }

  td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
  }

  tr:nth-child(even) {
      background-color: #dddddd;
  }
  </style>
  <style type="text/css">
  .page-break {
    page-break-after: always;
}
</style>
<body>
<center><h1>Rekapitulasi Data Barang</h1>
<h3>Bulan Ke-{{$bulan}}<h3>
</center>
<p> </p>
  <table>
    <tr>
      <th>Nama Barang</th>
      <th>Kode Barang</th>
      <th>Harga Satuan Barang</th>
      <th>Isi Barang</th>
      <th>Jenis Barang</th>
      <th>Diinput Oleh</th>
    </tr>
    @foreach($data as $key)
    <tr>
      <td>{{ $key->barang_nama }}</td>
      <td>{{ $key->barang_kode }}</td>
      <td>{{ $key->barang_harga }}</td>
      <td>{{ $key->barang_isi }}</td>
      <td>{{ $key->barang_jenis }}</td>
      <td>{{ $key->user_id }}</td>
    </tr>
    @endforeach
  </table>
<script src="{{ asset('js/table.js') }}"></script>
</body>
</html>