@extends('layouts.app-user')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Stock Barang
      <small>Form Data</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Barang</a></li>
      <li><a href="{{ url('superuser/inventory/stock') }}"></i> Stock Barang</a></li>
      <li class="active"> {{ $data1->barang_nama }}</li>
    </ol>
  </section>
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">{{ $data1->barang_nama }}</h3>
        <div class="pull-right">
          <a href="{{ url('#') }}" class="btn btn-default" id="buttonAdd">Tambah Stock</a>
        </div>
          @if(count($errors) > 0)
            @foreach($errors->all() as $error)
              <h4 style="color:red;">{{ $error }}</h4>
            @endforeach
          @endif
      </div>
      <div class="col-sm-12">
        <img class="img-responsive" style="width: 10%; height: 5%;" src="{{ url('images/'.$data1->barang_gambar)}}" alt="Photo">
      </div>
      <div class="box-body">
        <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Kode Barang</th>
          <th>Isi Barang</th>
          <th>Jenis Barang</th>
          <th>Pilihan</th>
        </tr>
        @php $i = 1; @endphp
        @foreach($data as $key)
          <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $key->barang_nama }}</td>
            <td>{{ $key->barang_kode }}</td>
            <td>{{ $key->barang_isi }}</td>
            <td>{{ $key->barang_jenis }}</td>
            <td>
              <center>
                <a href="{{ url('superuser/inventory/stock/delete/'.$key->id) }}" class="btn bg-red">Hapus</a>
                <a href="{{ url('#') }}" class="btn bg-green" onclick="modalEdit({{ $key->barang_kode }})">Ubah</a>
              </center>
            </td>
          </tr>
        @endforeach
        </table>
      </div>
      <div class="box-footer clearfix"></div>
    </div>
  </div>
</div>
    <div class="example-modal">
        <div class="modal" id="modalAdd">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambahkan Persediaan </h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal" method="POST" action="{{ url('superuser/inventory/stock/new') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $data1->id }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_nama" value="{{ $data1->barang_nama }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_kode"  placeholder="Masukkan Kode Barang">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    </div>

    <div class="example-modal">
        <div class="modal" id="modalEdit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Ubah Persediaan </h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal" method="POST" action="{{ url('superuser/inventory/stock/update') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $data1->id }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_nama" value="{{ $data1->barang_nama }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_kode" placeholder="Masukkan Kode Barang" id="kodeBarangEdit">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    </div>
</section>
</div>
<script type="text/javascript">
function modalEdit(id) {
  $("#kodeBarangEdit").val(id);
  $("#modalEdit").modal();
}

  $(function(){
    $('#buttonAdd').on('click', function(){
      $('#modalAdd').modal();
    });
  });
</script>
@endsection