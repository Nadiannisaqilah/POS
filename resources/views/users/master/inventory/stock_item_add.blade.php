@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Barang Beli
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Barang</a></li>
        <li class="active"> Barang Beli</li>
      </ol>
    </section>
  <section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Input Barang Baru</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ url('superuser/inventory/stock/save') }}" enctype="multipart/form-data"> 
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_nama" placeholder="Masukkan Nama Barang">
                  </div>
                </div>
                @if(count($errors) > 0)
                  @foreach($errors->all() as $error)
                    {{ $error }}
                  @endforeach
                @endif
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_kode"  placeholder="Masukkan Kode Barang">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Harga Barang</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" name="barang_harga" placeholder="Masukkan Harga Barang" id="barangHarga">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Jenis Barang</label>
                    <div class="col-sm-3">
                      <select class="form-control" name="barang_jenis">
                        <option disabled="" selected="">Pilih Jenis Barang</option>
                      @foreach($type as $key)
                        <option value="{{ $key->nama }}">{{ $key->nama }}</option>
                      @endforeach
                      </select>
                    </div>
                </div>               

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Isi Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="barang_isi" id="inputEmail3" placeholder="Isi satuan (kilogram, gram, liter, mililiter)">
                  </div>
                </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="exampleInputFile">Gambar</label>
                <input class="col-sm-10" type="file" name="barang_gambar" id="exampleInputFile">
              </div>

              <div class="box-footer">
                <button type="submit" id="buttonTambah" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Tambahkan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- <script type="text/javascript">
  $(function() {
    // $("input[type=number]").bind('keyup', function() {
    //   var check = this.value;
    //   if (check.length > 0) {
    //     var harga = $("#barangHarga").val();
    //     var jumlah = $("#barangJumlah").val();
    //     var total = harga*total;
    //     console.log(total);
    //   }
    // });
    $("#barangHarga").on('keyup', function() {
      var harga = this.value;
      var jumlah = $("#barangJumlah").val();
      if (harga.length>0 && jumlah.length>0) {
        var total = harga*jumlah;
        $("#grandTotal").html('Rp. '+total);
        $("#barangTotal").val(total);
      }
      else {
        $("#grandTotal").html('Rp. 0');
      }
    });

    $("#barangJumlah").on('keyup', function() {
      var jumlah = this.value;
      var harga = $("#barangHarga").val();
      if (harga.length>0 && jumlah.length>0) {
        var total = harga*jumlah;
        $("#grandTotal").html('Rp. '+total);
        $("#barangTotal").val(total); 
      }
      else {
        $("#grandTotal").html('Rp. 0');
      }
    });
  })
</script> -->
@endsection