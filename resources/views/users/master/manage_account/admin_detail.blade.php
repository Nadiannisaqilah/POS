@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Inventaris
        <small>Detail Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Akun</a></li>
        <li class="active">Inventaris</li>
      </ol>
    </section>
    <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body box-profile">
         <img class="profile-user-img img-responsive img-circle" src="{{ url('images/'.$data->image )}}" alt="User profile picture">

          <h2 class="profile-username text-center">{{ $data->name }}</h2>

          <h4><p class="text-muted text-center">{{ $data->email }}</p></h4>
        @php
          $role = \App\User::find($data->id)->roles()->first();
        @endphp
        <h4>
          <ul class="list-group list-group-unbordered ">
            <li class="list-group-item">
              <b>Status</b> <a class="pull-right">{{ $role->display_name }}</a>
            </li>
            <li class="list-group-item">
              <b>Jenis Kelamin</b> <a class="pull-right">{{ $data->gender }}</a>
            </li>
            <li class="list-group-item">
              <b>Nomor Telepon</b> <a class="pull-right">{{ $data->phone_number }}</a>
            </li>
            <li class="list-group-item">
              <b>Alamat</b> <a class="pull-right">{{ $data->address }}</a>
            </li>
          </ul>
        </h4>
            <a href="{{ url('superuser/manage/admin') }}" class="btn bg-blue"><i class="fa fa-arrow-left"></i> Kembali</a>
          <div class="pull-right">
            <a href="{{ url('superuser/manage/admin/edit/'.$data->id) }}" class="btn bg-green"><i class="fa fa-edit"></i> Ubah</a>
            <a href="{{ url('superuser/manage/admin/delete/'.$data->id)}}" class="btn bg-red"><i class="fa fa-eraser" onclick="return confirm('Yakin ingin menghapus data?');"></i> Hapus</a>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
     </div>
    </div>
   </section>
  </div>
@endsection