@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Inventaris
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Akun</a></li>
        <li class="active">Inventaris</li>
      </ol>
    </section>
  <section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Perbarui Data Inventaris</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ url('superuser/manage/user/update') }}" enctype="multipart/form-data"> 
             {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $data->id }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{ $data->name }}" name="name" id="inputEmail3" placeholder="Nama Lengkap">
                  </div>
                </div>
                @if(count($errors) > 0)
                  @foreach($errors->all() as $error)
                    {{ $error }}
                  @endforeach
                @endif
                
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                      <div class="col-sm-3">
                        <select class="form-control" name="gender">
                          <option value="Perempuan">Perempuan</option>
                          <option value="Laki-laki">Laki-laki</option>
                        </select>
                      </div>
                  </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nomor Telepon</label>
                  <div class="col-sm-10">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input type="text" name="phone_number" value="{{ $data->phone_number }}" class="form-control" id="telepon">
                  </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" rows="3" placeholder="Enter ...">{{ $data->address }}</textarea>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" value="{{ $data->email }}" name="email" id="inputEmail3" placeholder="Email">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>

              <div class="form-group">
               <center><img class="profile-user-img img-responsive img-circle" src="{{ url('images/'.$data->image )}}" alt="User profile picture"></center>
                <label class="col-sm-2 control-label" for="exampleInputFile">Image</label>
                <input class="col-sm-10" type="file" name="image" id="exampleInputFile">
              </div>

              <div class="box-footer">
                <button type="submit" id="buttonTambah" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Perbarui</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  (function() {
    $("#password").on('keyup', function() {
      var password = this.value;
      if (password.length > 0) {
        if (password != $("#password_confirmation").val()) {
          $("#buttonTambah").attr('disabled', '');
        }
        else {
          $("#buttonTambah").removeAttr("disabled");
        }
        $("#password_confirmation").removeAttr("disabled");
      }
      else {
        $("#password_confirmation").attr('disabled', '');
        $("#password_confirmation").val('');
      }
    });

    $("#password_confirmation").on('keyup', function() {
      var check = this.value;
      if (check == $("#password").val()) {
        $("#buttonTambah").removeAttr("disabled");
      }
      else {
        $("#buttonTambah").attr('disabled', '');
      }
    });

    $("#inputEmail3").on('keyup', function() {
      var inputEmail3 = this.value;
      if (inputEmail3.length > 0) {
        $("#buttonTambah").attr('disabled', '');
      }
      else{
        $("#buttonTambah").removeAttr("disabled");
      }
    });
  }())

</script>
<script type="text/javascript">
 $(document).ready(function ($) {
   $('#telepon').mask('(0000) 0000-0000');
 });
</script>
@endsection