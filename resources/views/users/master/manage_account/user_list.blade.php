@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Teller
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kelola Akun</a></li>
        <li class="active">Teller</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Data Teller</h3>
              <a href="{{ url('superuser/manage/user/create') }}" class="btn bg-blue" style="float: right;"><i class="fa fa-plus"></i></a>
              <a href="{{ url('report/teller/pdf') }}" style="float: right;" class="btn bg-red">
                <i class="fa fa-file-pdf-o"></i>
              </a>
              <a href="{{ url('report/teller/xlsx') }}" style="float: right;" class="btn bg-green">
                <i class="fa fa-file-excel-o"></i>
              </a>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key)
                 @php
                  $data = \App\User::find($key->id)->roles()->first();
                @endphp
                <tr>
                  <td>{{$key->name}}</td>
                  <td>{{$key->email}}</td>
                  <td>{{$data->display_name}}</td>
                  <td>
                  <center>
                    <a href="{{ url('/superuser/manage/user/detail/'.$key->id) }}" class="btn bg-blue">Detail</a>
                  </center>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
             </div>
            </div>
           </div>
         </div>
        </section>
       </div>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
@endsection