<!DOCTYPE html>
<html>
<head>
</head>
<style>
  table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
  }

  td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
  }

  tr:nth-child(even) {
      background-color: #dddddd;
  }
  </style>
  <style type="text/css">
  .page-break {
    page-break-after: always;
}
</style>
<body>
<center><h1>Rekapitulasi Data Anggota</h1>
<h3>Data {{$level}}<h3>
</center>
<p> </p>
  <table>
    <tr>
      <th>Nama</th>
      <th>Email</th>
      <th>Nomor Telepon</th>
      <th>Alamat</th>
      <th>Status</th>
    </tr>
    @foreach($data as $key)
    <tr>
      <td>{{ $key->name }}</td>
      <td>{{ $key->email }}</td>
      <td>{{ $key->phone_number }}</td>
      <td>{{ $key->address }}</td>
      <td>{{ $level }}</td>
    </tr>
    @endforeach
  </table>
<script src="{{ asset('js/table.js') }}"></script>
</body>
</html>