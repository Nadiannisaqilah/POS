@extends('layouts.app-user')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Transaksi
        <small>Form Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('superuser/activity') }}"><i class="fa fa-star"></i> Aktifitas</a></li>
        <li class="active">Kelola Transaksi</li>
      </ol>
    </section>
<section class="content">
<div class="row">
    <div class="col-sm-3">
      <form role="form">
        <div class="form-group">
          <!-- <h4><i class="fa fa-barcode fa-2x"></i></h4> -->
          <input type="text" class="form-control input-lg" id="barangKode" placeholder="Masukkan Kode Barang">
        </div>
      </form>
    </div>
        <div class="col-sm-9">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hitung Belanja</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th>Isi Barang</th>
                    <th>Harga Satuan Barang</th>
                    <th>Jumlah Barang</th>
                  </tr>
                </thead>
                <tbody id="tBody">
                  @if(session('transaksi'))
                  @php
                  $array = session('transaksi');
                  foreach ($array as $key => $value) {
                    $tmp[$key] = $value->barang_nama;
                  }

                  $tmp = array_unique($tmp);

                  foreach ($array as $key => $value) {
                    if (!array_key_exists($key, $tmp)) {
                      unset($array[$key]);
                    }
                  }
                  @endphp
                    @foreach($array as $key => $value)
                    <tr>
                      <td>{{ $value->barang_nama }}</td>
                      <td>{{ $value->barang_isi }}</td>
                      <td>Rp {{ $value->barang_harga }}</td>
                      <td>{{ $value->count }}</td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            <div class="col-sm-12">
              <div class="pull-right col-sm-6">
                <div class="box-body">
                  <h4>Pajak 10% :
                    <div class="pull-right" id="pajak">
                      @if(session('pajak'))
                        Rp. {{ session('pajak') }}
                      @else
                        Rp. 0
                      @endif
                    </div>
                  </h4>
                </div>
                <div class="box-body">
                  <h4>Total :
                    <div class="pull-right" id="total">
                      @if(session('jumlah'))
                        Rp. {{ session('jumlah') }}
                      @else
                        Rp. 0
                      @endif
                    </div>
                  </h4>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="box-body ">
              <div class="pull-right">
                <a href="{{ url('#') }}" class="btn btn-primary" id="buttonPay"><i class="fa fa-money margin-r-5"></i> Proses Pembayaran</a>
              </div>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <div class="example-modal">
        <div class="modal" id="modalPay">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title">Pembayaran Tunai</h3>
              </div>
              <div class="modal-body">
              <form class="form-horizontal" method="GET" action="{{ url('#') }}" enctype="multipart/form-data">
              <!-- <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-lg" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-lg" placeholder="Masukkan Nilai Pembayaran">
                  </div>
                </div>
                 <div class="form-group">
                  <div class="col-sm-12">
                    <center><b><h3 id="totalPay">Rp. 0</h3></b></center>
                  </div>
                </div>
              </div> -->
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-lg pull-left" data-dismiss="modal">Close</button>
                <a href="#" class="btn btn-default btn-lg"><i class="fa fa-print margin-r-5"></i>Print</a>
                <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-paper-plane-o margin-r-5"></i>Feedback</button>
              </div>
            </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $(function(){
    $('#barangKode').keyup(_.debounce(function(){
       var check = this.value;
       if (check.length > 0) {
         $.ajax({
           url : '{{ url('api/get/item') }}/'+check,
           method : 'GET',
           success: function(data){
             if (data.success == 0) {
               alert(data.message);
             }
             else{
               $("#tBody").html('');
               // alert(data.barang.barang_nama);
               $.each(data.barang, function(key, value) {
                 $("#tBody").append('<tr><td>'+value.barang_nama+'</td><td>'+value.barang_isi+'</td><td>Rp '+value.barang_harga+'</td><td>'+value.count+'</td></tr>');
               });

               $("#pajak").html('Rp. '+data.pajak);
               $("#total").html('Rp. '+data.total);
             }
             $("#barangKode").val('');
           }
         })
       }
    }, 500));
     $('#buttonPay').on('click', function(){
      $('#modalPay').modal();
     });
  });
</script>
@endsection
