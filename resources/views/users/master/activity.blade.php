@extends('layouts.app-user')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Kelola
      <small>Aktifitas</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Activity</li>
    </ol>
  </section>
<section class="content">
<div class="row">
  <a href="{{ url('superuser/transaction') }}" class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border bg-teal">
        <center><h3>Transaksi</h3></center>
      </div>
    <div class="box-body">
    <center>
      <i class="fa fa-exchange fa-5x"></i>
    </center>
    </div>
  </div>
</a>
<a href="{{ url('superuser/inventory/stock') }}" class="col-md-6">
    <div class="box box-solid">
    <div class="box-header with-border bg-teal">
        <center><h3>Inventaris</h3></center>
    </div>
    <div class="box-body">
    <center>
      <i class="fa fa-database fa-5x"></i>
    </center>
    </div>
  </div>
</a>
<a href="{{ url('superuser/manage/user') }}" class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border bg-teal">
        <center><h3>Akun Teller</h3></center>
      </div>
    <div class="box-body">
    <center>
      <i class="fa fa-user fa-5x"></i>
    </center>
    </div>
  </div>
</a>
<a href="{{ url('superuser/manage/admin') }}" class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border bg-teal">
        <center><h3>Akun Inventaris</h3></center>
      </div>
    <div class="box-body">
    <center>
      <i class="fa fa-user fa-5x"></i>
    </center>
    </div>
  </div>
</a>
</div>
</section>
<section class="content-header">
  <h1>
    Laporan
    <small>Aktifitas</small>
  </h1>
</section>
<section class="content">
<div class="row">
<a href="{{ url('superuser/inventory/stock/report') }}" class="col-md-4">
    <div class="box box-solid">
      <div class="box-header with-border bg-navy">
        <center><h3>History Barang</h3></center>
      </div>
    <div class="box-body">
    <center>
      <i class="fa fa-hourglass fa-2x"></i>
      <i class="fa fa-suitcase fa-5x"></i>
    </center>
    </div>
  </div>
</a>
<a href="{{ url('#') }}" class="col-md-4">
    <div class="box box-solid">
      <div class="box-header with-border bg-navy">
        <center><h3>History Transaksi</h3></center>
      </div>
    <div class="box-body">
    <center>
    <i class="fa fa-hourglass fa-2x"></i>
      <i class="fa fa-exchange fa-5x"></i>
    </center>
    </div>
  </div>
</a>
<a href="{{ url('#') }}" class="col-md-4">
    <div class="box box-solid">
      <div class="box-header with-border bg-navy">
        <center><h3>History Keuangan</h3></center>
      </div>
    <div class="box-body">
    <center>
    <i class="fa fa-hourglass fa-2x"></i>
      <i class="fa fa-money fa-5x"></i>
    </center>
    </div>
  </div>
</a>
</div>
</section>
</div>
@endsection