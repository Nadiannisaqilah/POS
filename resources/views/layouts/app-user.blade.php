<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panel Pos</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="shortcut icon" href="{{url('/landing/assets/images/favicon.png')}}">
  <link rel="stylesheet" href="{{ url('/master/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="https://cdnjsj .cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
  <link rel="stylesheet" href="{{ url('/master/plugins/datatables/dataTables.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ url('/master/plugins/datatables/dataTables.bootstrap.css') }}">

  <link rel="stylesheet" href="{{ url('/master/plugins/daterangepicker/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ url('/master/plugins/datepicker/datepicker3.css') }}">

  <link rel="stylesheet" href="{{ url('/master/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ url('/master/dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ url('/master/plugins/iCheck/flat/blue.css') }}">
  <link rel="stylesheet" href="{{ url('/master/plugins/morris/morris.css') }}">

  <link rel="stylesheet" href="{{ url('/master/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">

  <script src="{{ url('/master/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mask.js') }}"></script>
  <script src="{{ asset('js/underscore.js') }}"></script>
  <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
@php
  $user = \App\User::find(Auth::user()->id)->roles()->first();
@endphp

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>POS</b></span>
      <span class="logo-lg">Panel <b>POS</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ url('images/'.Auth::user()->image) }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="{{url('images/'.Auth::user()->image)}}" class="img-circle" alt="User Image">
                <p>
                  {{Auth::user()->name}} <br>
                  {{$user->display_name}}
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

@if(Auth::user()->hasRole('superuser'))
  @include('layouts.menu.superuser')
@elseif(Auth::user()->hasRole('admin'))
  @include('layouts.menu.admin')
@else
  @include('layouts.menu.user')
@endif

  @yield('content')


<script src="{{ url('/master/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ url('/master/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('/master/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ url('/master/plugins/morris/morris.min.js') }}"></script>
<script src="{{ url('/master/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ url('/master/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('/master/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('/master/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('/master/dist/js/app.min.js') }}"></script>
<script src="{{ url('/master/dist/js/demo.js') }}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
  if(event.keyCode == 13) {
    event.preventDefault();
    return false;
  }
});
});
</script>
</html>
