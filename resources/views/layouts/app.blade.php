<!DOCTYPE html>
<html>
  <head>    
    <meta charset="UTF-8">
   <title>Retail'POS</title>
    <meta name="description" content="Creative Web and Software Development Agency. Cutting Edge Technology and Creative Team.">
    <meta name="author" content="CodePassenger">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{url('/landing/assets/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" href="{{url('/landing/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('/landing/assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{url('/landing/assets/css/TimeCircles.css')}}">
    <!-- Font Awesome -->
    <link href="{{url('/landing/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Added google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700|Lobster|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <!--Fav and touch icons-->
    <link rel="shortcut icon" href="{{url('/landing/assets/images/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{url('/landing/assets/images/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('/landing/assets/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('/landing/assets/images/apple-touch-icon-114x114.png')}}">

  </head>           
<body>
@yield('content')   
</body>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{url('/landing/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('/landing/assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{url('/landing/assets/js/TimeCircles.js')}}"></script>
    <script src="{{url('/landing/assets/js/jquery.placeholder.js')}}"></script>
    <script src="{{url('/landing/assets/js/script.js')}}"></script>
    <script type="text/javascript">
        $(function() {
                // Invoke the plugin
                $('input, textarea').placeholder();
            });
    </script>
    <script>
      $("#count-down").TimeCircles(
       {   
           circle_bg_color: "#639094",
           use_background: true,
           bg_width: 1,
           fg_width: 0.02,
           time: {
                Days: { color: "#fefeee" },
                Hours: { color: "#fefeee" },
                Minutes: { color: "#fefeee" },
                Seconds: { color: "#fefeee" }
            }
       }
    );
    </script>
</body>
</html>
